/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('gogoTaskAngular')
    .constant('baseUrl', 'https://boiling-cliffs-96474.herokuapp.com/')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
