(function() {
  'use strict';

  angular
    .module('gogoTaskAngular')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
