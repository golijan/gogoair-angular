(function() {
  'use strict';

  angular
    .module('gogoTaskAngular')
    .config(config);

  /** @ngInject */
  function config($logProvider) {
    // Enable log
    $logProvider.debugEnabled(true);

  }

})();
