(function () {
  'use strict';

  angular
    .module('gogoTaskAngular')
    .controller('AddNewCandidateController', AddNewCandidateController);

  /** @ngInject */
  function AddNewCandidateController($uibModalInstance, CandidatesService) {
    var vm = this;

    vm.cancel = cancel;
    vm.addCandidate = addCandidate;

    function cancel() {
      $uibModalInstance.dismiss();
    }

    function addCandidate() {
      CandidatesService.addCandidate(vm.candidate).success(function () {
        $uibModalInstance.close();
      })
    }


  }
})();
