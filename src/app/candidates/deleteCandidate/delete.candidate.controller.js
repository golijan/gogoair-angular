(function() {
  'use strict';

  angular
    .module('gogoTaskAngular')
    .controller('DeleteCandidateController', DeleteCandidateController);

  /** @ngInject */
  function DeleteCandidateController(candidate, $uibModalInstance, CandidatesService, $log) {
    var vm = this;

    vm.candidate = candidate;

    vm.confirmDeletion = confirmDeletion;
    vm.cancel = cancel;

    function confirmDeletion() {
      CandidatesService.deleteCandidate(vm.candidate._id).success(function () {
        $log.log('deleted successfully');
        $uibModalInstance.close();
      });
    }

    function cancel() {
      $uibModalInstance.dismiss();
    }


  }
})();
