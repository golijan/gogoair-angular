(function () {
  'use strict';

  angular
    .module('gogoTaskAngular')
    .controller('EditCandidateController', EditCandidateController);

  /** @ngInject */
  function EditCandidateController($uibModalInstance, candidate, CandidatesService, $log) {
    var vm = this;

    vm.cancel = cancel;
    vm.candidate = candidate;
    vm.editCandidate = editCandidate;

    function cancel() {
      $uibModalInstance.dismiss();
    }

    function editCandidate(candidate) {
      CandidatesService.editCandidate(candidate).success(function (data) {
        $log.log('edited successfully', data);
        $uibModalInstance.close();
      })
    }

  }
})();
