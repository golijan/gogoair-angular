(function () {
  'use strict';

  angular
    .module('gogoTaskAngular')
    .controller('NotesController', NotesController);

  /** @ngInject */
  function NotesController($uibModalInstance, candidate, CandidatesService) {
    var vm = this;

    vm.cancel = cancel;
    vm.addNote = addNote;

    vm.candidate = candidate;

    function cancel() {
      $uibModalInstance.dismiss();
    }

    function addNote(id) {
      CandidatesService.addNote(id, vm.newNote).success(function () {
        candidate.notes.push({author: 'Admin', comment: vm.newNote});
        vm.newNote = '';
      })
    }

  }
})();
