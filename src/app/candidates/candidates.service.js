(function () {
  'use strict';

  angular
    .module('gogoTaskAngular')
    .service('CandidatesService', CandidatesService);

  /** @ngInject */
  function CandidatesService($http, baseUrl, $log) {
    var service = this;

    service.getCandidates = getCandidates;
    service.deleteCandidate = deleteCandidate;
    service.editCandidate = editCandidate;
    service.addCandidate = addCandidate;
    service.addNote = addNote;

    function getCandidates(data) {
      return $http({
        method: "GET",
        url: baseUrl + 'candidates',
        params: {
          page: data.page,
          size: data.size,
          sortBy: data.sortBy,
          sortDir: data.sortDir
        }
      }).success(function (data) {
        return data;
      }).error(function (errorData) {
        $log.log(errorData);
      })

    }

    function deleteCandidate(id) {
      return $http({
        method: "DELETE",
        url: baseUrl + 'candidates/' + id
      }).success(function (data) {
        return data;
      }).error(function (errorData) {
        $log.log(errorData);
      })

    }

    function editCandidate(candidate) {
      return $http({
        method: "PUT",
        url: baseUrl + 'candidates/' + candidate._id,
        data: {
          candidate: candidate
        }
      }).success(function (data) {
        return data;
      }).error(function (errorData) {
        $log.log(errorData);
      })

    }

    function addCandidate(candidate) {
      return $http({
        method: "POST",
        url: baseUrl + 'candidate',
        data: {
          candidate: candidate
        }
      }).success(function (data) {
        return data;
      }).error(function (errorData) {
        $log.log(errorData);
      })

    }

    function addNote(candidateId, comment) {
      return $http({
        method: "POST",
        url: baseUrl + 'candidate/' + candidateId + '/note',
        data: {
          note: {
            author: 'Admin',
            comment: comment
          }
        }
      }).success(function (data) {
        return data;
      }).error(function (errorData) {
        $log.log(errorData);
      })

    }

  }
})();
