(function () {
  'use strict';

  angular
    .module('gogoTaskAngular')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($uibModal, CandidatesService) {
    var vm = this;

    vm.openAddNewCandidate = openAddNewCandidate;
    vm.openEditCandidate = openEditCandidate;
    vm.deleteCandidate = deleteCandidate;
    vm.openCandidateNotes = openCandidateNotes;
    vm.nextPage = nextPage;
    vm.previousPage = previousPage;
    vm.sortCandidates = sortCandidates;
    vm.toggleDirection = toggleDirection;

    vm.params = {
      page: 0,
      size: 10,
      sortBy: 'date',
      sortDir: 'asc'
    };


    activate();

    // vm.candidates = [
    //   {
    //     "id": 1,
    //     "first_name": "Petar",
    //     "last_name": "Petrovic",
    //     "skills": [{"text": "javascript"}, {"text": "html5"}, {"text": "css3"}],
    //     "date": "2017-03-04",
    //     "cv_uri": "http://somedomain.com/cvs/junior-fe", "cv_file_name": "PetarPetrovic.pdf",
    //     "notes": []
    //   },
    //   {
    //     "id": 2,
    //     "first_name": "Jovan",
    //     "last_name": "Jovanovic",
    //     "skills": [{"text": "es6"}, {"text": "react"}, {"text": "redux"}],
    //     "date": "2017-03-17",
    //     "cv_uri": "http://somedomain.com/cvs/senior-fe", "cv_file_name": "Jovan_Jovanovic.pdf",
    //     "notes": [
    //       {
    //         "author": "Brandan Eich",
    //         "comment": "Great candidate with solid understanding of..."
    //       }, {
    //         "author": "HR department",
    //         "comment": "Good communication skills..."
    //       }
    //     ]
    //   },
    //   {
    //     "id": 3,
    //     "first_name": "Simo",
    //     "last_name": "Matavulj",
    //     "skills": [{"text": "jQuery"}, {"text": "angular"}, {"text": "css3"}],
    //     "date": "2017-03-14",
    //     "cv_uri": "http://somedomain.com/cvs/junior-fe", "cv_file_name": "Matavulj_Frontend.pdf",
    //     "notes": []
    //   }];

    function activate() {
      getCandidates()
    }


    function getCandidates() {
      var data = angular.copy(vm.params);
      CandidatesService.getCandidates(data).success(function (data) {
        vm.candidates = data;
      })
    }

    function openAddNewCandidate() {
      var modalInstance = $uibModal.open({
        animation: true,
        templateUrl: 'app/candidates/addNew/add-new.html',
        controller: 'AddNewCandidateController',
        controllerAs: 'addNew',
        size: 'lg'

      });

      modalInstance.result.then(
        function () {
          getCandidates();
        })
    }

    function openEditCandidate(candidate) {
      var modalInstance = $uibModal.open({
        animation: true,
        templateUrl: 'app/candidates/editCandidate/edit-candidate.html',
        controller: 'EditCandidateController',
        controllerAs: 'edit',
        size: 'lg',
        resolve: {
          candidate: function () {
            return candidate;
          }
        }

      });

      modalInstance.result.then(
        function () {
          getCandidates();
        })
    }

    function deleteCandidate(candidate) {
      openDeletionModal('md', candidate);
    }

    function openDeletionModal (size, candidate) {
      var modalInstance = $uibModal.open({
        animation: true,
        templateUrl: 'app/candidates/deleteCandidate/delete-candidate.html',
        controller: 'DeleteCandidateController',
        controllerAs: 'deleteCandidate',
        size: size,
        resolve: {
          candidate: function () {
            return candidate;
          }
        }
      });

      modalInstance.result.then(
        function () {
          getCandidates();
        })
    }

    function openCandidateNotes (candidate) {
      var modalInstance = $uibModal.open({
        animation: true,
        templateUrl: 'app/candidates/notes/notes.html',
        controller: 'NotesController',
        controllerAs: 'notes',
        size: 'lg',
        resolve: {
          candidate: function () {
            return candidate;
          }
        }
      });
      modalInstance.result.then(
        function () {

        })
    }

    function nextPage() {
      vm.params.page++;
      getCandidates();
    }

    function previousPage() {
      vm.params.page--;
      getCandidates();
    }

    function sortCandidates(sortBy) {
      vm.params.sortBy = sortBy;
      getCandidates();
    }
    
    function toggleDirection() {
      vm.params.sortDir = vm.params.sortDir === 'asc' ? 'desc' : 'asc';
      getCandidates();
    }

  }
})();
