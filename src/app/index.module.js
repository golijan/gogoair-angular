(function() {
  'use strict';

  angular
    .module('gogoTaskAngular', ['ui.router', 'ui.bootstrap', 'ngTagsInput']);

})();
